# README #

## Installation

Firstly, the graph-package has a couple of dependencies installed by:

`sudo apt install graphviz libgraphviz-dev libgraph6`

Secondly, move into the `contact_map` directory and run `make` followed by running
the `make_lib.sh` script.

Lastly, simply setup the conda environment with the environment.yml file and activate
it whenever you wish to run InterPepRank:

`conda env create -f environment.yml`

`conda activate interpeprank`

## Running InterPepRank

While in the InterPepRank main directory, run `scripts/create_graphrep.py` to process 
a batch of models with the same receptor and peptide. This will create a representation 
readable by the graph-net.

After you have your graph-readable file, run `scripts/apply_nets.py` on said file to 
make the InterPepRank prediction.

### Example

To test your installation of InterPepRank, you can run the following commands:

`scripts/create_graphrep.py example/example_list.txt example/receptor.psi example/peptide.fa -r example/receptor.pdb`

`scripts/apply_nets.py test.h5 example/example_list.txt --basetargets`

and compare the resulting `ipr_out.csv` with the `example/expected_output.csv`.

## Input

The input to InterPepRank consists of a list of PDB-files, a sequence file for the 
peptide in FASTA format (> header row included), and a multiple sequence alignment for
the receptor in .psi format. To acquire the MSA, the following command-line is 
recommended (requires you have hhsuite installed):

`hhblits -i <receptor_sequence> -opsi <psi_file> -all -n 2`

## Re-training the nets

If you wish to re-train the InterPepRank nets using compatible data, you will be able to
do so using the `scripts/train_nets.py` script (CURRENTLY BEING IMPLEMENTED).
