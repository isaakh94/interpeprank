#!/usr/bin/env python

#    train_nets.py part of the InterPepRank protocol
#    Copyright (C) 2020  Isak Johansson-@Aring;khe
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import numpy as np
np.random.seed(42)
import tensorflow as tf
from tensorflow import set_random_seed
set_random_seed(42)
import copy
from keras.models import Model
from keras.utils import Sequence
from keras import layers, callbacks
from keras.layers import Dropout, Lambda
from keras.optimizers import Adam
import keras.backend as K
import h5py
import random
random.seed(42)
import sys
import re
import pandas as pd
from glob import glob
from spektral.layers import GlobalAvgPool, GlobalAttentionPool, EdgeConditionedConv
from apply_nets import DataGenerator, Model_0, Model_1, Model_2, Model_3, Model_4, Model_7, Model_8
import argparse

def classifier_multiclass_recall(y_true, y_pred):
    y_true_bin = tf.dtypes.cast(K.clip(K.argmax(y_true, axis = 1), 0, 1), tf.float32)
    y_pred_bin = tf.dtypes.cast(K.clip(K.argmax(y_pred, axis = 1), 0, 1), tf.float32)
    return K.sum(y_true_bin*y_pred_bin)/K.clip(K.sum(y_true_bin), 1e-7, None)

def classifier_multiclass_precision(y_true, y_pred):
    y_true_bin = tf.dtypes.cast(K.clip(K.argmax(y_true, axis = 1), 0, 1), tf.float32)
    y_pred_bin = tf.dtypes.cast(K.clip(K.argmax(y_pred, axis = 1), 0, 1), tf.float32)
    return K.sum(y_true_bin*y_pred_bin)/K.clip(K.sum(y_pred_bin), 1e-7, None)

def cross_entropy_numpy(y_true, y_pred):
    y_pred /= y_pred.sum(axis=-1, keepdims=True)
    y_pred = np.clip(y_pred, 1e-7, 1 - 1e-7)
    return np.average(np.sum(y_true * -np.log(y_pred), axis = -1, keepdims = False))

def multiclass_precision_numpy(y_true, y_pred, threshold_class):
    y_pred_bin = (y_pred == y_pred.max(axis = 1)[:,None]).astype(float)
    return np.sum(y_true*y_pred_bin)/np.clip(np.sum(y_pred_bin), 1e-7, None)

def multiclass_recall_numpy(y_true, y_pred, threshold_class):
    y_pred_bin = (y_pred == y_pred.max(axis = 1)[:,None]).astype(float)
    return np.sum(y_true*y_pred_bin)/np.clip(np.sum(y_true), 1e-7, None)

def main():
    parser = argparse.ArgumentParser('train_nets.py Copyright (C) 2020 Isak Johansson-@Aring;khe\nThis program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE file.')
    parser.add_argument('input', type = str, help = 'h5 file containing graphs to run on')
    parser.add_argument('trainlist', type = str, help = 'The list of all targets to train on')
    parser.add_argument('targetvalues', type = str, help = 'Text file with target values for targets in input file. The file should be csv-file with the first column denoting target names and the second denoting target label values.')
    parser.add_argument('-v', '--validlist', type = str, default = '', help = 'The list of all targets to validate on. If empty, a random subset of the trainlist will be held out for validation.')
    parser.add_argument('--basetargets', action = 'store_true', 
                        help = 'Set if you didn\'t use a prefix for your targets')
    parser.add_argument('--output_dir', type = str, default = '', help = 'Directory to put weights files in.')
    parser.add_argument('--validation_frequency', type = int, default = 1, help = 'How many epochs should pass between each validation and save of weights.')
    parser.add_argument('--net', type = int, default = 0, help = 'Which net should be trained')
    parser.add_argument('-c', '--cpu', type = int, default = 1)
    parser.add_argument('--epochs', type = int, default = 100, help = 'Number of epochs to train for')
    parser.add_argument('--batch_size', type = int, default = 16)
    args = parser.parse_args()
    
    print('train_nets.py Copyright (C) 2020 Isak Johansson-@Aring;khe\nThis program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE file.')
    
    use_multiprocessing = False
    if args.cpu > 1:
        use_multiprocessing = True
    args.output_dir = os.path.abspath(args.output_dir) + '/'
        
    #Loading list of validation and train targets
    traintargets = []
    with open(args.trainlist, 'r') as f:
        traintargets = f.read().split()
        if args.basetargets:
            traintargets = [target.split('/')[-1] for target in traintargets]
    if not args.validlist == '':
        validtargets = []
        with open(args.validlist, 'r') as f:
            validtargets = f.read().split()
            if args.basetargets:
                validtargets = [target.split('/')[-1] for target in validtargets]
    else:
        validtargets_indexes = np.random.randint(0, len(traintargets), int(0.2*len(traintargets)))
        validtargets = [traintargets[i] for i in validtargets_indexes]
        traintargets = [j for i, j in enumerate(traintargets) if not i in validtargets_indexes]
    print(traintargets[:10])
    
    #Loading target values
    facit_dict = {}
    with open(args.targetvalues, 'r') as f:
        lines = [line.split(',') for line in f.readlines()[1:]]
        for line in lines:
            facit_dict[line[0]] = float(line[1])
            
    #Making sure all train and validation targets have associated target values
    def check_targets(targetlist, values_dict):
        tmp = []
        for target in targetlist:
            try:
                values_dict[target]
                tmp.append(target)
            except:
                print('{} missing target value!'.format(target))
        return tmp
    traintargets = check_targets(traintargets, facit_dict)
    validtargets = check_targets(validtargets, facit_dict)
    
    #Loading the architecture of the net to train
    models = {0: (Model_0, 4), 
              1: (Model_1, 3), 
              2: (Model_2, 2), 
              3: (Model_3, 2), 
              4: (Model_4, 2), 
              7: (Model_7, 4), 
              8: (Model_8, 2)}
    try:
        model, n_classes = models[args.net]
    except Exception as e:
        print('Please select an existing network architecture!')
        raise e
    mod = model()
    mod.compile(optimizer = Adam(lr = 0.001), loss = 'categorical_crossentropy', metrics = [classifier_multiclass_recall, classifier_multiclass_precision])
    
    #Training net
    weight_string = 'weights{}_'.format(args.net) + '{epoch:04d}.h5'
    csv_string = 'ipr_out_valid{}'.format(args.net) + '_{:04d}.csv'
    mc = callbacks.ModelCheckpoint(weight_string, save_weights_only=True, period = args.validation_frequency)
    for epoch in range(args.epochs):
        training_generator = DataGenerator(args.input, traintargets, 
                                           classes = n_classes, facit = facit_dict, batch_size = args.batch_size)
        mod.fit_generator(generator = training_generator,
                          epochs = epoch+1, 
                          initial_epoch = epoch,
                          shuffle = True,
                          use_multiprocessing=use_multiprocessing,
                          workers = args.cpu,
                          callbacks = [mc])
        del training_generator #You might be wondering why we delete the generators after every epoch. Well, turns out that on our machines training with large amounts of data would unexpectedly seize up after some epochs unless this is done regularly, with no prior warning or deterioration in training quality.
        if not epoch % args.validation_frequency == 0:
            continue
        validation_generator = DataGenerator(args.input, validtargets, 
                                             classes = n_classes, facit = facit_dict, batch_size = 1, shuffle = False)
        predicted = mod.predict_generator(generator = validation_generator, 
                                          verbose = 1, 
                                          use_multiprocessing = use_multiprocessing, 
                                          workers = args.cpu)
        del validation_generator
        val_true = np.asarray([facit_dict[i] for i in validtargets], dtype = np.float32)
        increment = 1.0 / n_classes
        onehot_labels = np.identity(n_classes)
        midbin = increment * np.arange(n_classes) + increment/2
        val_predicted_pos = np.sum(predicted * midbin, axis=1)
        val_true_bin = []
        for element in validtargets:
            class_index = int(min(n_classes - 1, facit_dict[element] // increment))
            val_true_bin.append(list(onehot_labels[class_index]))
        val_true_bin = np.asarray(val_true_bin)
        val_loss = cross_entropy_numpy(val_true_bin, predicted)
        threshold_class = np.argmin(np.abs(0.5 - increment * np.arange(n_classes)))
        val_recall = multiclass_recall_numpy(val_true_bin, predicted, threshold_class)
        val_precision = multiclass_precision_numpy(val_true_bin, predicted, threshold_class)
        print('val_loss: {}, val_recall: {}, val_precision: {}'.format(val_loss, val_recall, val_precision))
        with open(csv_string.format(epoch), 'w') as f:
            f.write('target,predicted,true\n')
            for label, prediction, true in zip(validtargets, val_predicted_pos, val_true):
                f.write('{},{},{}\n'.format(label, prediction, true))

if __name__ == '__main__':
    main()