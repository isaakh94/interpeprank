#!/usr/bin/env python

#    apply_nets.py part of the InterPepRank protocol
#    Copyright (C) 2020  Isak Johansson-@Aring;khe
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import numpy as np
np.random.seed(42)
import tensorflow as tf
from tensorflow import set_random_seed
set_random_seed(42)
import copy
from keras.models import Model
from keras.utils import Sequence
from keras import layers
from keras.layers import Dropout, Lambda
import keras.backend as K
import h5py
import random
random.seed(42)
import sys
import re
import pandas as pd
from glob import glob
from spektral.layers import GlobalAvgPool, GlobalAttentionPool, EdgeConditionedConv

import argparse

class DataGenerator(Sequence):
    """
    Data generator
    """

    def __init__(self, h5, list_IDs, batch_size=1, dim=100, n_channels=64,
                 shuffle=True, classes=None, epoch_size = None, facit = None):
        """
        Initialization
        """
        self.dim = dim #Dimensions of representation
        self.n_channels = n_channels #Number of channels of representation
        self.batch_size = batch_size #Batch size
        self.list_IDs = list_IDs #All IDs in list form
        self.shuffle = shuffle #Should the targets be shuffled?
        self.h5 = h5 #Path to database containing representations
        self.edge_features = 3 #If we want to include edge-features in representation
        self.classes = classes #If we want one-hot encoded y
        self.indexes = np.arange(len(self.list_IDs))
        self.epoch_size = epoch_size #If we want a set epoch size
        self.on_epoch_end()
        
        self.labels = {}
        if self.classes and facit:
            onehot_labels = np.identity(self.classes)
            increment = 1.0/self.classes
            for key in facit.keys():
                this_class = int(facit[key]//increment)
                self.labels[key] = onehot_labels[this_class]
        else:
            self.labels = facit

    def __len__(self):
        """
        Denotes the number of batches per epoch
        Can be set by user
        """
        if self.epoch_size:
            return self.epoch_size
        else:
            return int(np.floor(len(self.list_IDs) / self.batch_size))
    
    def __getitem__(self, index):
        """
        Generate one batch of data by calling __data_generation
        """
        
        #Indexes
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        
        #List of IDs
        list_of_IDs_temp = [self.list_IDs[k] for k in indexes]

        X, y = self.__data_generation(list_of_IDs_temp)
        return X, y
    
    def on_epoch_end(self):
        """
        Updates indexes after each epoch
        """
        if self.shuffle == True:
            self.indexes = np.arange(len(self.list_IDs))
            np.random.shuffle(self.indexes)
    
    def __data_generation(self, list_IDs_temp):
        """
        Grabs data containing batch_size samples.
        """
        Edge = np.zeros((self.batch_size, self.dim, self.dim))
        Data = np.zeros((self.batch_size, self.dim, self.n_channels))
        Iden = np.zeros((self.batch_size, self.dim, self.dim, self.edge_features))
        y = np.empty((self.batch_size, self.classes))
        
        #Getting data
        inputfile = h5py.File(self.h5, 'r')
        for i, ID in enumerate(list_IDs_temp):
            try:
                Data[i,...] = inputfile[ID + '/Data'][()]
                Edge[i,...] = inputfile[ID + '/Edge'][()]
                Iden[i,...] = inputfile[ID + '/Iden'][()]
                if self.labels:
                    y[i,...] = self.labels[ID]
            except Exception as e:
                print(str(e) + ' ' + ID)
                Data[i,...] = np.zeros((self.dim, self.n_channels))
                Edge[i,...] = np.zeros((self.dim, self.dim))
                Iden[i,...] = np.zeros((self.dim, self.dim, self.edge_features))
        inputfile.close()
        
        return [Data, Edge, Iden], y

def Model_0(input_shape=[100, 64, 3], weights = None, classes = 4):
    activation = 'relu'
    data_input = layers.Input(shape = (input_shape[0], input_shape[1]), name='data')
    edge_input = layers.Input(shape = (input_shape[0], input_shape[0]), name='edges')
    cond_input = layers.Input(shape = (input_shape[0], input_shape[0], input_shape[2]), name = 'edge_features')
    data_input_pssm_entropy = Lambda(lambda x: x[:, :, :42])(data_input)
    data_input_dummy = Lambda(lambda x: x[:, :, 63:])(data_input)
    data_input_ligand = Lambda(lambda x: x[:, :, 42:63])(data_input)
    data_input_ligand_argmax = Lambda(lambda x: K.argmax(x))(data_input_ligand)
    data_input_ligand_embed = layers.Embedding(22, 4)(data_input_ligand_argmax)
    data_input_remerged = layers.Concatenate()([data_input_pssm_entropy, data_input_ligand_embed, data_input_dummy])
    conv1 = EdgeConditionedConv(4)([data_input_remerged, edge_input, cond_input])
    conv1 = layers.Activation(activation)(conv1)
    conv2 = EdgeConditionedConv(4)([conv1, edge_input, cond_input])
    conv2 = layers.Activation(activation)(conv2)
    conv3 = EdgeConditionedConv(8)([conv2, edge_input, cond_input])
    conv3 = layers.Activation(activation)(conv3)
    conv4 = EdgeConditionedConv(8)([conv3, edge_input, cond_input])
    conv4 = layers.Activation(activation)(conv4)
    x = layers.Concatenate()([conv1, conv2, conv3, conv4])
    x = GlobalAvgPool()(x)
    x = layers.Dense(32)(x)
    x = layers.Activation(activation)(x)
    x = layers.Dense(classes)(x)
    out = layers.Activation('softmax')(x)
    model = Model(inputs = [data_input, edge_input, cond_input], outputs = out)
    if weights:
        model.load_weights(weights)
    return model

def Model_1(input_shape=[100, 64, 3], weights = None, classes = 3):
    activation = 'relu'
    data_input = layers.Input(shape = (input_shape[0], input_shape[1]), name='data')
    edge_input = layers.Input(shape = (input_shape[0], input_shape[0]), name='edges')
    cond_input = layers.Input(shape = (input_shape[0], input_shape[0], input_shape[2]), name = 'edge_features')
    data_input_pssm_entropy = Lambda(lambda x: x[:, :, :42])(data_input)
    data_input_dummy = Lambda(lambda x: x[:, :, 63:])(data_input)
    data_input_ligand = Lambda(lambda x: x[:, :, 42:63])(data_input)
    data_input_ligand_argmax = Lambda(lambda x: K.argmax(x))(data_input_ligand)
    data_input_ligand_embed = layers.Embedding(22, 4)(data_input_ligand_argmax)
    data_input_remerged = layers.Concatenate()([data_input_pssm_entropy, data_input_ligand_embed, data_input_dummy])
    conv1 = EdgeConditionedConv(4)([data_input_remerged, edge_input, cond_input])
    conv1 = layers.Activation(activation)(conv1)
    conv2 = EdgeConditionedConv(4)([conv1, edge_input, cond_input])
    conv2 = layers.Activation(activation)(conv2)
    conv3 = EdgeConditionedConv(8)([conv2, edge_input, cond_input])
    conv3 = layers.Activation(activation)(conv3)
    conv4 = EdgeConditionedConv(8)([conv3, edge_input, cond_input])
    conv4 = layers.Activation(activation)(conv4)
    x = layers.Concatenate()([conv1, conv2, conv3, conv4])
    x = GlobalAvgPool()(x)
    x = layers.Dense(8)(x)
    x = layers.Activation(activation)(x)
    x = layers.Dense(classes)(x)
    out = layers.Activation('softmax')(x)
    model = Model(inputs = [data_input, edge_input, cond_input], outputs = out)
    if weights:
        model.load_weights(weights)
    return model

def Model_2(input_shape=[100, 64, 3], weights = None, classes = 2):
    activation = 'relu'
    data_input = layers.Input(shape = (input_shape[0], input_shape[1]), name='data')
    edge_input = layers.Input(shape = (input_shape[0], input_shape[0]), name='edges')
    cond_input = layers.Input(shape = (input_shape[0], input_shape[0], input_shape[2]), name = 'edge_features')
    data_input_pssm_entropy = Lambda(lambda x: x[:, :, :42])(data_input)
    data_input_dummy = Lambda(lambda x: x[:, :, 63:])(data_input)
    data_input_ligand = Lambda(lambda x: x[:, :, 42:63])(data_input)
    data_input_ligand_argmax = Lambda(lambda x: K.argmax(x))(data_input_ligand)
    data_input_ligand_embed = layers.Embedding(22, 4)(data_input_ligand_argmax)
    data_input_remerged = layers.Concatenate()([data_input_pssm_entropy, data_input_ligand_embed, data_input_dummy])
    conv1 = EdgeConditionedConv(4)([data_input_remerged, edge_input, cond_input])
    conv1 = layers.Activation(activation)(conv1)
    conv2 = EdgeConditionedConv(4)([conv1, edge_input, cond_input])
    conv2 = layers.Activation(activation)(conv2)
    conv3 = EdgeConditionedConv(8)([conv2, edge_input, cond_input])
    conv3 = layers.Activation(activation)(conv3)
    conv4 = EdgeConditionedConv(8)([conv3, edge_input, cond_input])
    conv4 = layers.Activation(activation)(conv4)
    x = layers.Concatenate()([conv1, conv2, conv3, conv4])
    x = GlobalAvgPool()(x)
    x = layers.Dense(16)(x)
    x = layers.Activation(activation)(x)
    x = layers.Dense(classes)(x)
    out = layers.Activation('softmax')(x)
    model = Model(inputs = [data_input, edge_input, cond_input], outputs = out)
    if weights:
        model.load_weights(weights)
    return model

def Model_3(input_shape=[100, 64, 3], weights = None, classes = 2):
    activation = 'relu'
    data_input = layers.Input(shape = (input_shape[0], input_shape[1]), name='data')
    edge_input = layers.Input(shape = (input_shape[0], input_shape[0]), name='edges')
    cond_input = layers.Input(shape = (input_shape[0], input_shape[0], input_shape[2]), name = 'edge_features')
    data_input_pssm_entropy = Lambda(lambda x: x[:, :, :42])(data_input)
    data_input_dummy = Lambda(lambda x: x[:, :, 63:])(data_input)
    data_input_ligand = Lambda(lambda x: x[:, :, 42:63])(data_input)
    data_input_ligand_argmax = Lambda(lambda x: K.argmax(x))(data_input_ligand)
    data_input_ligand_embed = layers.Embedding(22, 2)(data_input_ligand_argmax)
    data_input_remerged = layers.Concatenate()([data_input_pssm_entropy, data_input_ligand_embed, data_input_dummy])
    conv1 = EdgeConditionedConv(8)([data_input_remerged, edge_input, cond_input])
    conv1 = layers.Activation(activation)(conv1)
    conv2 = EdgeConditionedConv(8)([conv1, edge_input, cond_input])
    conv2 = layers.Activation(activation)(conv2)
    conv3 = EdgeConditionedConv(16)([conv2, edge_input, cond_input])
    conv3 = layers.Activation(activation)(conv3)
    conv4 = EdgeConditionedConv(16)([conv3, edge_input, cond_input])
    conv4 = layers.Activation(activation)(conv4)
    x = layers.Concatenate()([conv1, conv2, conv3, conv4])
    x = GlobalAvgPool()(x)
    x = layers.Dense(32)(x)
    x = layers.Activation(activation)(x)
    x = layers.Dense(classes)(x)
    out = layers.Activation('softmax')(x)
    model = Model(inputs = [data_input, edge_input, cond_input], outputs = out)
    if weights:
        model.load_weights(weights)
    return model
    
def Model_4(input_shape=[100, 64, 3], weights = None, classes = 2):
    activation = 'relu'
    data_input = layers.Input(shape = (input_shape[0], input_shape[1]), name='data')
    edge_input = layers.Input(shape = (input_shape[0], input_shape[0]), name='edges')
    cond_input = layers.Input(shape = (input_shape[0], input_shape[0], input_shape[2]), name = 'edge_features')
    data_input_pssm_entropy = Lambda(lambda x: x[:, :, :42])(data_input)
    data_input_dummy = Lambda(lambda x: x[:, :, 63:])(data_input)
    data_input_ligand = Lambda(lambda x: x[:, :, 42:63])(data_input)
    data_input_ligand_argmax = Lambda(lambda x: K.argmax(x))(data_input_ligand)
    data_input_ligand_embed = layers.Embedding(22, 2)(data_input_ligand_argmax)
    data_input_remerged = layers.Concatenate()([data_input_pssm_entropy, data_input_ligand_embed, data_input_dummy])
    conv1 = EdgeConditionedConv(8, kernel_network=[8])([data_input_remerged, edge_input, cond_input])
    conv1 = layers.Activation(activation)(conv1)
    conv2 = EdgeConditionedConv(8, kernel_network=[8])([conv1, edge_input, cond_input])
    conv2 = layers.Activation(activation)(conv2)
    conv3 = EdgeConditionedConv(16, kernel_network=[16])([conv2, edge_input, cond_input])
    conv3 = layers.Activation(activation)(conv3)
    conv4 = EdgeConditionedConv(16, kernel_network=[16])([conv3, edge_input, cond_input])
    conv4 = layers.Activation(activation)(conv4)
    x = layers.Concatenate()([conv1, conv2, conv3, conv4])
    x = GlobalAvgPool()(x)
    x = layers.Dense(32)(x)
    x = layers.Activation(activation)(x)
    x = layers.Dense(classes)(x)
    out = layers.Activation('softmax')(x)
    model = Model(inputs = [data_input, edge_input, cond_input], outputs = out)
    if weights:
        model.load_weights(weights)
    return model

def Model_7(input_shape=[100, 64, 3], weights = None, classes = 4):
    activation = 'relu'
    data_input = layers.Input(shape = (input_shape[0], input_shape[1]), name='data')
    edge_input = layers.Input(shape = (input_shape[0], input_shape[0]), name='edges')
    cond_input = layers.Input(shape = (input_shape[0], input_shape[0], input_shape[2]), name = 'edge_features')
    data_input_pssm_entropy = Lambda(lambda x: x[:, :, :42])(data_input)
    data_input_dummy = Lambda(lambda x: x[:, :, 63:])(data_input)
    data_input_ligand = Lambda(lambda x: x[:, :, 42:63])(data_input)
    data_input_ligand_argmax = Lambda(lambda x: K.argmax(x))(data_input_ligand)
    data_input_ligand_embed = layers.Embedding(22, 4)(data_input_ligand_argmax)
    data_input_remerged = layers.Concatenate()([data_input_pssm_entropy, data_input_ligand_embed, data_input_dummy])
    conv1 = EdgeConditionedConv(8, kernel_network= [8])([data_input_remerged, edge_input, cond_input])
    conv1 = layers.Activation(activation)(conv1)
    conv2 = EdgeConditionedConv(8, kernel_network = [8])([conv1, edge_input, cond_input])
    conv2 = layers.Activation(activation)(conv2)
    conv3 = EdgeConditionedConv(16, kernel_network = [16])([conv2, edge_input, cond_input])
    conv3 = layers.Activation(activation)(conv3)
    conv4 = EdgeConditionedConv(16, kernel_network = [16])([conv3, edge_input, cond_input])
    conv4 = layers.Activation(activation)(conv4)
    x = layers.Concatenate()([conv1, conv2, conv3, conv4])
    x = GlobalAttentionPool()(x)
    x = layers.Dense(32)(x)
    x = layers.Activation(activation)(x)
    x = layers.Dense(classes)(x)
    out = layers.Activation('softmax')(x)
    model = Model(inputs = [data_input, edge_input, cond_input], outputs = out)
    if weights:
        model.load_weights(weights)
    return model

def Model_8(input_shape=[100, 64, 3], weights = None, classes = 2):
    activation = 'relu'
    data_input = layers.Input(shape = (input_shape[0], input_shape[1]), name='data')
    edge_input = layers.Input(shape = (input_shape[0], input_shape[0]), name='edges')
    cond_input = layers.Input(shape = (input_shape[0], input_shape[0], input_shape[2]), name = 'edge_features')
    data_input_pssm_entropy = Lambda(lambda x: x[:, :, :42])(data_input)
    data_input_dummy = Lambda(lambda x: x[:, :, 63:])(data_input)
    data_input_ligand = Lambda(lambda x: x[:, :, 42:63])(data_input)
    data_input_ligand_argmax = Lambda(lambda x: K.argmax(x))(data_input_ligand)
    data_input_ligand_embed = layers.Embedding(22, 2)(data_input_ligand_argmax)
    data_input_remerged = layers.Concatenate()([data_input_pssm_entropy, data_input_ligand_embed, data_input_dummy])
    conv1 = EdgeConditionedConv(8, kernel_network=[8])([data_input_remerged, edge_input, cond_input])
    conv1 = layers.Activation(activation)(conv1)
    conv2 = EdgeConditionedConv(8, kernel_network=[8])([conv1, edge_input, cond_input])
    conv2 = layers.Activation(activation)(conv2)
    conv3 = EdgeConditionedConv(16, kernel_network=[16])([conv2, edge_input, cond_input])
    conv3 = layers.Activation(activation)(conv3)
    conv4 = EdgeConditionedConv(16, kernel_network=[16])([conv3, edge_input, cond_input])
    conv4 = layers.Activation(activation)(conv4)
    x = layers.Concatenate()([conv1, conv2, conv3, conv4])
    x = GlobalAttentionPool(128)(x)
    x = layers.Dense(32)(x)
    x = layers.Activation(activation)(x)
    x = layers.Dense(classes)(x)
    out = layers.Activation('softmax')(x)
    model = Model(inputs = [data_input, edge_input, cond_input], outputs = out)
    if weights:
        model.load_weights(weights)
    return model
    
def main():
    parser = argparse.ArgumentParser('apply_nets.py Copyright (C) 2020 Isak Johansson-@Aring;khe\nThis program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE file.')
    parser.add_argument('input', type = str, help = 'h5 file containing graphs to run on')
    parser.add_argument('inputlist', type = str, help = 'The list of all targets')
    parser.add_argument('--basetargets', action = 'store_true', 
                        help = 'Set if you didn\'t use a prefix for your targets')
    parser.add_argument('--separate_output', action = 'store_true', 
                        help = 'Set if you want the outputs from the nets as separate files')
    parser.add_argument('--weights_folder', type = str, default = '', 
                        help = 'Set if you have your own directory of weights')
    parser.add_argument('--custom_prefix', type = str, default = '', 
                        help = 'Set if you have a custom unrelated prefix for labels in the input.')
    parser.add_argument('-c', '--cpu', type = int, default = 1)
    parser.add_argument('-o', '--output', type = str, default = 'ipr_out.csv')
    args = parser.parse_args()
    
    print('apply_nets.py Copyright (C) 2020 Isak Johansson-@Aring;khe\nThis program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE file.')
    
    use_multiprocessing = False
    if args.cpu > 1:
        use_multiprocessing = True
    
    #Loading list of all targets
    targets = []
    with open(args.inputlist, 'r') as f:
        targets = f.read().split()
        if args.basetargets:
            targets = [target.split('/')[-1] for target in targets]
        if not args.custom_prefix == '':
            targets = [args.custom_prefix + target for target in targets]
    print(targets[:10])
    
    model_output = []
    models = [[Model_0, 'netweights/best_dropout025_4class_deeper_weights.h5'], 
              [Model_1, 'netweights/best_dropout025_3class_deeper_smalldense_weights.h5'], 
              [Model_2, 'netweights/best_dropout025_2class_deeper_16dense_weights.h5'], 
              [Model_3, 'netweights/best_dropout025_2class_2embed_8-8-16-16_32dense_weights.h5'], 
              [Model_4, 'netweights/best_dropout010_2class_kernel_net8_8-8-16-16_32dense_weights.h5'], 
              [Model_7, 'netweights/best_dropoutp025_4class_knet8_8-8-16-16_32dense_attention_weights.h5'], 
              [Model_8, 'netweights/best_dropoutp010_2class_knet8_8-8-16-16_attention_weights.h5']]
    classes_list = [4, 3, 2, 2, 2, 4, 2]
    modelnum = [0, 1, 2, 3, 4, 7, 8]
    if not args.weights_folder == '':
        w_folder_contents = glob(args.weights_folder + '/*.h5')
        for i, modnum in enumerate(modelnum):
            for wfile in w_folder_contents:
                if os.path.basename(wfile)[:8] == str(modnum) + 'weights':
                    models[i][1] = wfile
                    break
    modelcounter = 0
    for model, weight in models:
        classes = classes_list[modelcounter]
        mod = model(weights = weight, classes = classes)
        mod.summary()
        test_generator = DataGenerator(args.input, targets, shuffle=False, classes = classes)
        prediction = mod.predict_generator(generator = test_generator, 
                                           verbose = 1, 
                                           use_multiprocessing=use_multiprocessing, 
                                           workers = args.cpu)
        del test_generator
        increment = 1.0/classes
        midbin = increment*np.arange(classes) + increment/2
        prediction = np.sum(prediction*midbin, axis=1)
        if args.separate_output:
            with open(args.output + '_' + str(modelnum[modelcounter]), 'w') as f:
                f.write('target,predicted\n')
                for label, pred in zip(targets, prediction):
                    f.write('{},{}\n'.format(label, pred))
        model_output.append(prediction)
        modelcounter += 1
    combined_output = np.average(np.asarray(model_output), axis = 0)
    print('printing...')
    with open(args.output, 'w') as f:
        f.write('target,predicted\n')
        for label, prediction in zip(targets, combined_output):
            f.write('{},{}\n'.format(label, prediction))
    print('...done!')
    
if __name__ == '__main__':
    main()
