#!/usr/bin/env python

#    create_graphrep.py part of the InterPepRank protocol
#    Copyright (C) 2020  Isak Johansson-@Aring;khe
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import sys
import tempfile
from sklearn.preprocessing import OneHotEncoder
import os
import numpy as np
import h5py
from glob import glob

from ctypes import c_void_p, c_double, c_int, cdll, POINTER, cast
from numpy.ctypeslib import ndpointer, as_array
lib = cdll.LoadLibrary('contact_map/contact_map.so')
lib.free_dist_mat.argtypes = c_void_p, 
dist_mat = lib.dist_mat
dist_mat.restype = c_void_p

CONTACT_THR = 4.5
DIM = 100
COMPRESSION = 'gzip'

background = [3.75284752e-02,   1.23789120e-02,  8.20472216e-02,   6.57800639e-02,
              5.32205893e-02,   3.27212591e-02,  5.12586289e-02,   5.39336236e-02,
              3.08498057e-02,   4.99251634e-02,  4.15628301e-02,   2.11314529e-02,
              3.41916615e-02,   2.74236163e-02,  1.10874953e-02,   1.90467644e-02,
              8.95799830e-02,   5.33478686e-02,  6.68577999e-02,   4.70830993e-02,
              1.19015648e-01]

letter_to_number = { 'P':'0', 'C':'1', 'A':'2', 'G':'3', 'S':'4',
                     'N':'5', 'D':'6', 'E':'7', 'Q':'8', 'R':'9',
                     'K':'10', 'H':'11', 'F':'12', 'Y':'13', 'W':'14',
                     'M':'15', 'L':'16', 'I':'17', 'V':'18', 'T':'19',
                     '-':'20', 'X':'20' }

def make_graph(PSSM, ENTROPY, OHRECEPTOR, OHPEPTIDE, tmpfile, tmpfile2, receptor, struct):
    #Cutting together the file
    os.system('cat ' + receptor + ' > ' + tmpfile2)
    os.system('cat ' + struct + ' >> ' + tmpfile2)
    
    #Getting distance map
    """
    command = 'scripts/contact_map_chain' + ' ' + tmpfile2 + ' | grep -v "^$" > ' + tmpfile
    os.system(command)
    lines = []
    with open(tmpfile, 'r') as f:
        lines = f.readlines()[2:] #First 2 lines are unnecessary
    with open(tmpfile, 'w') as f:
        f.write('\n'.join([line.split(': ')[1][:-2] for line in lines]))
        #Everything before : is unwanted and lines have an extra space at the end
    dmap = np.loadtxt(tmpfile, delimiter = ' ') #Well this is slow
    """
    baseshape = PSSM.shape[0] + OHPEPTIDE.shape[0]
    d = dist_mat(tmpfile2.encode('utf-8'))
    dmap = as_array(cast(d, POINTER(c_double)), shape = (baseshape, baseshape))
    
    #Making feature-vectors
    #pssm for receptor and zeros for peptide
    datatmp = np.concatenate((PSSM, np.zeros((OHPEPTIDE.shape[0], PSSM.shape[1]))))
    #entropy for receptor and zeros for peptide
    datatmp = np.concatenate((datatmp, np.concatenate((ENTROPY, np.zeros((OHPEPTIDE.shape[0], ENTROPY.shape[1]))))), axis = 1)
    #one-hot-encoded sequence for both
    datatmp = np.concatenate((datatmp, np.concatenate((OHRECEPTOR, OHPEPTIDE))), axis = 1)
    #dummy variable peptide/not peptide
    dummy = np.zeros((PSSM.shape[0] + OHPEPTIDE.shape[0], 1))
    dummy[PSSM.shape[0]:,...] = 1.0
    datatmp = np.concatenate((datatmp, dummy), axis = 1)
    
    #Making edge identities
    emap = np.zeros((dmap.shape[0], dmap.shape[0], 3))
    emaptmp = np.roll(np.identity(dmap.shape[0]), 1, axis = 1)
    emaptmp = emaptmp + np.roll(np.identity(dmap.shape[0]), -1, axis = 1)
    emap[...,2] = emaptmp
    emap[0, -1, 2] = 0.0 #taking care of wrapping corners
    emap[-1, 0, 2] = 0.0
    emap[PSSM.shape[0],PSSM.shape[0]-1, 2] = 0.0 #Removing covalent binding between receptor and ligand
    emap[PSSM.shape[0]-1,PSSM.shape[0], 2] = 0.0
    #Making self-binding vector
    emap[...,0] = np.identity(dmap.shape[0])
    #Adding distance map
    emap[...,1] = np.sign(dmap-CONTACT_THR)/-2+0.5 - emap[...,0] - emap[...,2]
    emap[emap < 0.0] = 0.0
    
    #Removing distant residues to leave only 100 nodes
    threshold = sorted(np.min(dmap[..., PSSM.shape[0]:], axis = 1))[min(DIM, dmap.shape[0])-1]
    interface = np.unique(np.where(dmap[..., PSSM.shape[0]:] <= threshold)[0])
    datatmp = datatmp[interface,:]
    dmap = dmap[interface,:][:,interface]
    emap = emap[interface,:,...][:,interface,...]
    
    #Reduces distance-map to contact-map
    dmap = np.sign(dmap-CONTACT_THR)/-2+0.5
    
    #Making final matrices
    Edge = np.zeros((DIM, DIM))
    Data = np.zeros((DIM, datatmp.shape[1]))
    Iden = np.zeros((DIM, DIM, 3))
    Data[:datatmp.shape[0],:] = datatmp[-DIM:,:]
    Edge[:dmap.shape[0], :dmap.shape[1]] = dmap[-DIM:,-DIM:]
    Iden[:dmap.shape[0], :dmap.shape[1], ...] = emap[-DIM:,-DIM:,...]
    
    #Output
    lib.free_dist_mat(d)
    return [Data, Edge, Iden]

def letters_to_numbers_file(inputfile, outputfile):
    output = ''
    with open(inputfile, 'r') as f:
        for line in f:
            for char in line.rstrip():
                try:
                    output += letter_to_number[char] + ' '
                except KeyError as e:
                    output += 'X '
            output = output[:-1] + '\n'
    with open(outputfile, 'w') as f:
        f.write(output)

def main():
    parser = argparse.ArgumentParser('Script for creating graphs of peptide-protein complexes. Prior to running this script, make sure you have run HHBLITS on the receptor-structure and that all receptors are denoted as chain "A" and all peptides as chain "B".\n\ncreate_graphrep.py Copyright (C) 2020 Isak Johansson-@Aring;khe\nThis program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE file.')
    parser.add_argument('structures_list', type = str,
                        help = 'List of structures to run on.')
    parser.add_argument('psi_receptor', type = str, help = 'Path to .psi output from HHBLITS')
    parser.add_argument('seq_peptide', type = str, help = 'Path to sequence of peptide ligand')
    parser.add_argument('-r', '--receptor_struct', type = str, default = '', 
                        help = 'Set if the individual poses do not contain the receptor')
    parser.add_argument('-l', '--label_prefix', type = str, default = '', 
                        help = 'A prefix for the labels of the structures')
    parser.add_argument('-o', '--output', type = str, default = 'test.h5')
    args = parser.parse_args()
    
    print('create_graphrep.py Copyright (C) 2020 Isak Johansson-@Aring;khe\nThis program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE file.')
    
    TMP = '/tmp/'
    JOBID = ''
    if "SLURM_ARRAY_TASK_ID" in os.environ.keys():
        if not os.environ["SLURM_ARRAY_TASK_ID"] == 'only-set-in-job-environment':
            TMP = os.environ['SNIC_TMP'] + '/'
            JOBID = os.environ['SLURM_JOB_ID']

    struct_list = []
    with open(args.structures_list, 'r') as f:
        struct_list = f.read().split()
    
    #First, convert the alignment file into a .num file to get the number-encoded MSA
    aln_receptor = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    aln_receptor.close()
    aln_receptor = aln_receptor.name
    command = "awk '{ print $2 }' " + args.psi_receptor + ' > ' + aln_receptor
    os.system(command)
    num_receptor = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    num_receptor.close()
    num_receptor = num_receptor.name
    letters_to_numbers_file(aln_receptor, num_receptor)
    MSA = np.loadtxt(num_receptor, dtype = 'i8', delimiter = " ")
    MSA = np.transpose(MSA)
    
    #Making a PSSM and relative entropies
    master = ''
    with open(aln_receptor, 'r') as f:
        master = f.readline()[:-1]
    depth = MSA.shape[1]
    PSSM = np.zeros((len(master), 21))
    ENTROPY = np.zeros((len(master), 21))
    for i in range(len(master)):
        unique, counts = np.unique(np.append(MSA[i, :depth], [n for n in range(21)]), return_counts=True)
        frequencies = counts.astype('float32')/depth
        PSSM[i, :] = np.log(frequencies/background)
        ENTROPY[i, :] = frequencies*np.log(frequencies/background)
        
    #Getting one-hot-encoded receptor sequence
    tmprecept = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    tmprecept.close()
    tmprecept = tmprecept.name
    command = 'head -n 1 ' + num_receptor + ' > ' + tmprecept
    os.system(command)
    receptor_numb = np.genfromtxt(tmprecept, dtype='i8', delimiter=" ")
    os.unlink(tmprecept)
    encoder = OneHotEncoder(handle_unknown = 'ignore', sparse = False)
    encoder.fit(np.array([x for x in range(21)]).reshape(-1, 1))
    OHRECEPTOR = encoder.transform(receptor_numb[:,np.newaxis])
    
    #Then we get the one-hot-encoded peptide sequence
    seq_peptide = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    seq_peptide.close()
    seq_peptide = seq_peptide.name
    command = 'grep -v ">" ' + args.seq_peptide + ' > ' + seq_peptide
    os.system(command)
    num_peptide = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    num_peptide.close()
    num_peptide = num_peptide.name
    letters_to_numbers_file(seq_peptide, num_peptide)
    pept_numb = np.loadtxt(num_peptide, dtype='i8', delimiter=" ")
    encoder = OneHotEncoder(handle_unknown = 'ignore', sparse = False)
    encoder.fit(np.array([x for x in range(21)]).reshape(-1, 1))
    OHPEPTIDE = encoder.transform(pept_numb[:,np.newaxis])
    
    #Getting representation for all files and saving them
    outh5 = h5py.File(args.output, 'w')
    tmpfile = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    tmpfile.close()
    tmpfile = tmpfile.name
    tmpfile2 = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    tmpfile2.close()
    tmpfile2 = tmpfile2.name
    receptor = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    receptor.close()
    receptor = receptor.name
    if args.receptor_struct:
        os.system('cp ' + args.receptor_struct + ' ' + receptor)
    for struct in struct_list:
        print(struct)
        sys.stdout.flush()
        label = args.label_prefix + os.path.basename(struct)
        graph = make_graph(PSSM, ENTROPY, OHRECEPTOR, OHPEPTIDE, tmpfile, tmpfile2, receptor, struct)
        data = outh5.create_dataset(label + '/Data',
                                    graph[0].shape, dtype = 'float32', compression = COMPRESSION)
        data[...] = graph[0]
        edge = outh5.create_dataset(label + '/Edge',
                                    graph[1].shape, dtype = 'float32', compression = COMPRESSION)
        edge[...] = graph[1]
        iden = outh5.create_dataset(label + '/Iden',
                                    graph[2].shape, dtype = 'float32', compression = COMPRESSION)
        iden[...] = graph[2]
        outh5.flush()
    
    #Cleaning
    outh5.close()
    os.unlink(aln_receptor)
    os.unlink(num_receptor)
    os.unlink(seq_peptide)
    os.unlink(num_peptide)
    os.unlink(tmpfile)
    os.unlink(tmpfile2)
    os.unlink(receptor)
    
if __name__ == '__main__':
    main()
