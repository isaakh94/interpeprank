#!/usr/bin/env python3
from ctypes import c_void_p, c_double, c_int, cdll, POINTER, cast
from numpy.ctypeslib import ndpointer, as_array
import numpy as np
import time
import os

lib = cdll.LoadLibrary("/proj/wallner/users/x_isaak/contactmap/contact_map.so")
lib.free_dist_mat.argtypes=c_void_p,
dist_mat=lib.dist_mat
dist_mat.restype=c_void_p ##POINTER(c_double)
#dist_mat.restype=ndpointer(dtype=c_double,shape=(261,261))
#print(dist_mat.restype)

counter = 0
timestart = time.time()
for _ in range(100):
    counter+=1
    a = '../interpepdeep/data/testing/testfile.pdb'
    #a = 'apa.pdb'
    print(counter)
    #"""
    d=dist_mat(a.encode('utf-8'))
    mat=cast(d,POINTER(c_double))
    #mat=as_array(mat, shape=(261,261))
    mat=as_array(mat, shape=(125,125))
    #print(len(mat))
    #print(mat.shape)
    lib.free_dist_mat(d)
    """
    #print(mat)
    #print(mat[-2][-2])
    tmpfile = 'tempi'
    command = './contact_map_chain ' + a + ' | grep "^RES" > ' + tmpfile
    os.system(command)
    lines = []
    with open(tmpfile, 'r') as f:
        lines = f.readlines()#[2:] #First 2 lines are unnecessary
    with open(tmpfile, 'w') as f:
        f.write('\n'.join([line.split(': ')[1][:-2] for line in lines]) + '\n') #Everything before : is unwanted
    dmap = np.loadtxt(tmpfile, delimiter = ' ')
    """
    
    #os.system('./contact_map_chain apa.pdb > tmpfile.txt')
print('TIME')
print(time.time()-timestart)
