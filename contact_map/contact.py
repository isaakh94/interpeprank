#!/usr/bin/env python3

from ctypes import c_void_p, c_double, c_int, cdll,POINTER,cast
from numpy.ctypeslib import ndpointer, as_array
import numpy as np
import time

lib = cdll.LoadLibrary("contact_map.so")
lib.free_dist_mat.argtypes=c_void_p,
##lib.free_dist_mat.restype=None
dist_mat=lib.dist_mat
dist_mat.restype=c_void_p ##POINTER(c_double)
#dist_mat.restype=ndpointer(dtype=c_double,shape=(261,261))

#print(dist_mat.restype)

d=dist_mat(b'apa.pdb')

print("In python:",hex(d))
mat=cast(d,POINTER(c_double))

mat2=as_array(mat,shape=(261,261))
lib.free_dist_mat(d)
print(mat2)
#print(len(d))
#print(d[0][1])
#print(hex(d))

